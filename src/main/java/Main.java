import com.rockaport.alice.Alice;
import com.rockaport.alice.AliceContext;
import com.rockaport.alice.AliceContextBuilder;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a file path: ");
//        /home/matata/Programacion/cripto/out/artifacts/cripto/a.txt
        System.out.flush();
        String filename = scanner.nextLine();
        System.out.print("Enter a password: ");
        System.out.flush();
        String key = scanner.nextLine();
        System.out.println("Enter the number for the option desire:");
        System.out.println("1: Encrypt");
        System.out.println("2: Decript");
        System.out.flush();
        int opcion = Integer.valueOf(scanner.nextLine());
        File file = new File(filename);
//        Instancio librería Alice la cual cifra en AES 256 segun su documentación
        Alice alice = new Alice(new AliceContextBuilder().build());
        AliceContext aliceContext = new AliceContextBuilder()
                .setAlgorithm(AliceContext.Algorithm.AES)
                .setMode(AliceContext.Mode.GCM)
                .setIvLength(12) // e.g. 12
                .setGcmTagLength(AliceContext.GcmTagLength.BITS_128)
                .build();
        Path path = Paths.get(file.getAbsolutePath());
        if(Files.exists(path) && !key.isEmpty()){
            char[] password = key.toCharArray();
            File encriptadoFile = null;
            switch (opcion) {
                case 1:
                    encriptadoFile = new File(file.getAbsolutePath()+"-encriptado.txt");                    ;
                    alice.encrypt(file, encriptadoFile, password);
                    System.out.println("Se a creado el archivo encriptado en la misma carpeta del archivo seleccionado");
                    break;
                case 2:
                    encriptadoFile = new File(file.getAbsolutePath()+"-encriptado.txt");
                    File desencriptadoFile = new File(file.getAbsolutePath()+"-desencriptado.txt");
                    alice.decrypt(encriptadoFile, desencriptadoFile, password);
                    System.out.println("Se a creado el archivo desencriptado en la misma carpeta del archivo seleccionado");
                    break;
                default: System.out.println("Opción incorrecta");
                    break;
            }
        }
    }
}